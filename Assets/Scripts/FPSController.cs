﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

public class FPSController : MonoBehaviour
{
    public float gravity = 9.8f;
    private float vSpeed = 0;
    
    public float speed = 4;
    public float jumpForce = 5;
    
    public float mouseSensitivity = 3;

    public GameObject cameraObject;
    private float rotX;
    
    private CharacterController characterController;
    private Rigidbody rigidbody;

    private Vector3 knockbackVector;
    private PlayerHealth playerHealth;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        playerHealth = GetComponent<PlayerHealth>();
    }

    
    void Update()
    {
        if (!playerHealth.isDead)
        {
            // Get input dir
            Vector3 dir = Input.GetAxis("Horizontal") * transform.right + Input.GetAxis("Vertical") * transform.forward;
            dir = dir.normalized;

            // Rotate horizontal
            if (Input.GetAxis("Mouse X") != 0)
            {
                transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * mouseSensitivity, 0));
            }
        
            // Rotate vertical
            if (Input.GetAxis("Mouse Y") != 0)
            {
                rotX -= Input.GetAxis("Mouse Y") * mouseSensitivity;
                rotX = Mathf.Clamp(rotX, -90, 90);
                Quaternion rotation = Quaternion.Euler(rotX, 0, 0);
            
                cameraObject.transform.localRotation = rotation;
            }

            // if / else player is on the ground
            if (!characterController.isGrounded)
            {
                vSpeed -= gravity * Time.deltaTime;
            }
            else
            {
                // Jump
                if (Input.GetButtonDown("Jump"))
                    vSpeed = jumpForce;
            }
            
            // Decrease knockback vector if there is one
            if (knockbackVector != Vector3.zero)
            {
                if (knockbackVector.magnitude > .2)
                    knockbackVector = Vector3.Lerp(knockbackVector, Vector3.zero, .05f);
                else
                    knockbackVector = Vector3.zero;
            }
        
            // Apply movement
            characterController.Move((dir * speed + Vector3.up * vSpeed + knockbackVector) * Time.deltaTime);
        }
    }

    public void ApplyKnockbackVector(Vector3 newVector)
    {
        knockbackVector = newVector;
        vSpeed = 0;
    }
}
