﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth;
    public int health;
    public float invicibleTime = 1.5f;

    private float timer;

    public RectTransform healthBar;
    
    private float healthBarScaleTarget;
    private FPSController fpsController;
    private Coroutine barCoroutine;

    public bool isDead = false;

    void Start()
    {
        health = maxHealth;
        fpsController = GetComponent<FPSController>();
    }

    private void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
    }

    public void Damage(int amount, Vector3 sourcePos, float knockbackForce)
    {
        if (isDead != true)
        {
            if (timer <= 0)
            {
                timer = invicibleTime;
            
                health -= amount;

                if (health <= 0)
                {
                    health = 0;
                    Death();
                }

                UpdateHealthBar();

                Vector3 newVector = transform.position - sourcePos;
                newVector = newVector.normalized * knockbackForce;

                fpsController.ApplyKnockbackVector(new Vector3(newVector.x, knockbackForce, newVector.z));
            }
        }
    }

    void UpdateHealthBar()
    {
        healthBarScaleTarget = (float)health / (float)maxHealth;
        
        if (barCoroutine != null)
            StopCoroutine(barCoroutine);
        barCoroutine = StartCoroutine(SmoothBar());
    }

    IEnumerator SmoothBar()
    {
        while (Mathf.Abs(healthBarScaleTarget - healthBar.localScale.x) > .01f)
        {
            float newScale = Mathf.Lerp(healthBar.localScale.x, healthBarScaleTarget, .1f);
            
            healthBar.localScale = new Vector3(newScale, 1, 1);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    
    void Death()
    {
        isDead = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        FindObjectOfType<ButtonActionScript>().GameOverScreenSwitch();
    }
}
