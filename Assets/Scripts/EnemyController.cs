﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyController : MonoBehaviour
{
    public int maxHealth;
    private int health;
    public RectTransform healthBar;
    public GameObject healthBarObject;

    [Header("Attack")] 
    public float aggroDistance = 15f;
    public float attackDistance = 10f;
    public float attackCooldown = 5f;
    public UnityEvent attackCallback;

    public Animator animator;
    public Rigidbody mainRb;
    private Vector3 lastHitDir;

    private float cooldown;
    private GameObject player;
    private FollowPath followPath;

    public bool dead = false;

    void Start()
    {
        // Set some variables
        health = maxHealth;
        player = FindObjectOfType<PlayerHealth>().gameObject;
        followPath = GetComponent<FollowPath>();

        if (dead)
        {
            animator.enabled = false;
            healthBarObject.SetActive(false);
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    private void Update()
    {
        if (animator.enabled)
            animator.SetFloat("Velocity", Vector3.Magnitude(GetComponent<NavMeshAgent>().velocity));

        if (!dead)
        {
            followPath.followPath = Vector3.Distance(player.transform.position, transform.position) >= aggroDistance;
        
            // Attack
            cooldown -= Time.deltaTime;
            if (Vector3.Distance(player.transform.position, transform.position) < attackDistance && cooldown <= 0)
            {
                cooldown = attackCooldown;
                attackCallback.Invoke(); // Unity event is use to change the main attack of the enemy in case we need
            }
        }
    }

    public void Damage(int amount, Vector3 dir)
    {
        health -= amount;
        if (health <= 0) health = 0;
        
        healthBar.localScale = new Vector3((float)health / (float)maxHealth, 1, 1);

        lastHitDir = dir;
        
        if (health <= 0)
            if (!dead)
                Death();
    }

    public void Death()
    {
        dead = true;
        animator.enabled = false;
        healthBarObject.SetActive(false);
        GetComponent<CapsuleCollider>().enabled = false;
        
        mainRb.AddForce(lastHitDir * 20);
    }
}
