﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    private Transform player;
    
    void Start()
    {
        player = FindObjectOfType<PlayerHealth>().transform;
    }
    void Update()
    {
        transform.LookAt(player);
    }
}
