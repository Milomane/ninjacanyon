﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject projectile;
    public Transform projectileSpawn;
    public float attackCooldown = 1.5f;

    public float power = 15;

    private float cd;
    
    void Update()
    {
        if (!GetComponent<PlayerHealth>().isDead)
        cd -= Time.deltaTime;
        
        if (Input.GetButtonDown("Fire") && cd <= 0)
        {
            cd = attackCooldown;
            Shoot();
        }
    }

    public void Shoot()
    {
        GameObject proj = Instantiate(projectile, projectileSpawn.position,  Camera.main.transform.rotation);
        proj.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * power, ForceMode.Impulse);
        proj.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0, 360, 0));
    }
}
