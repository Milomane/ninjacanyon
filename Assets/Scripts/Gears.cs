﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gears : MonoBehaviour
{
    public static Gears gears;

    public Camera cam;

    public UIManager uiManager;

    public Player_Equipment PlayerEquipment;

    public UI_PlayerEquipment UiPlayerEquipment;

    public LayerMask barrelLayer;


    [Header("Prefab")] 
    public GameObject equipmentName;

    void Awake()
    {
        if (gears == null)
        {
            DontDestroyOnLoad(gameObject);
            gears = this;
        }
        else if (gears != null)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }
    
    public void LoadMenu()
    {
        
    }

    public void LoadGame()
    {
        
    }

    public void Quit()
    {
        Application.Quit();
    }
}
