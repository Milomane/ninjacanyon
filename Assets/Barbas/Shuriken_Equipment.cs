﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuriken_Equipment : Equipment
{
    public Shuriken_Equipment(Player_Equipment playerEquipment) : base(playerEquipment)
    {
        equipmentName = "Shuriken";
    }
    public override void UseEquipment()
    {
        playerEquipment.gameObject.GetComponent<PlayerShoot>().Shoot();
    }
}
