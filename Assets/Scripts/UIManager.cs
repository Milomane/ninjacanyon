﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public Canvas canvasMain;

    public GameObject textHolder;

    public GameObject uiPlayerEquipmentsHolder;

    void Awake()
    {
        Gears.gears.uiManager = this;
    }
    
    void Start()
    {
        canvasMain = GetComponent<Canvas>();
    }
    
    void Update()
    {
        
    }
}
