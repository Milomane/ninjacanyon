﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment
{
   public Player_Equipment playerEquipment;

   public string equipmentName;
   
   public Equipment(Player_Equipment playerEquipment)
   {
      this.playerEquipment = playerEquipment;
   }
   public virtual void UseEquipment()
   {
      
   }
}
