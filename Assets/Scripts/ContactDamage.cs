﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactDamage : MonoBehaviour
{
    public int damage = 1;
    public int knockbackForce = 1;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerHealth>() && !GetComponent<EnemyController>().dead)
        {
            other.GetComponent<PlayerHealth>().Damage(damage,transform.position, knockbackForce);
        }
    }
}
