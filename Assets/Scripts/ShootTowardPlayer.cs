﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTowardPlayer : MonoBehaviour
{
    public GameObject prefabShuriken;
    public Transform bulletSpawn;

    public float power = 10;
    public float addedHeight;

    private GameObject player;

    private void Start()
    {
        player = FindObjectOfType<PlayerHealth>().gameObject;
    }

    // Shoot is call in Enemy controller
    public void Shoot()
    {
        GameObject o = Instantiate(prefabShuriken, bulletSpawn.position, Quaternion.identity);

        float height = addedHeight * (Vector3.Distance(player.transform.position, transform.position) / 10);
        
        Vector3 vectorTowardPlayer = (player.transform.position + new Vector3(0, height, 0)) - transform.position;
        
        o.GetComponent<Rigidbody>().AddForce(vectorTowardPlayer * power);
        o.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0, 360, 0));
    }
}
