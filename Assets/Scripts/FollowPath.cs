﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class FollowPath : MonoBehaviour
{
    public Transform[] pathsObject;
    public int index;
    
    public float objectifDistance;
    public float stopFollowPlayerDistance = 10;

    public bool followPath;
    public bool wasNotFollowing;
    
    private NavMeshAgent agent;
    private Transform player;

    private void Start()
    {
        player = FindObjectOfType<PlayerHealth>().transform;
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(pathsObject[index].position + new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f)));
    }

    void Update()
    {
        if (!GetComponent<EnemyController>().dead)
        {
            if (followPath)
                FollowNormalPath();
            else
                FollowPlayer();
        }
        else
            agent.isStopped = true;
    }

    void FollowNormalPath()
    {
        if (wasNotFollowing)
        {
            agent.isStopped = false;
            wasNotFollowing = false;
            float dist = 999999999;
            for (int i = 0; i < pathsObject.Length; i++)
            {
                if (Vector3.Distance(transform.position, pathsObject[i].transform.position) < dist)
                {
                    index = i;
                    dist = Vector3.Distance(transform.position, pathsObject[i].transform.position);
                }
            }

            agent.SetDestination(pathsObject[index].position + new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f)));
        }
        else
        {
            if (Vector3.Distance(agent.destination, transform.position) <= objectifDistance)
            {
                index++;
                if (index >= pathsObject.Length)
                    index = 0;

                agent.SetDestination(pathsObject[index].position + new Vector3(Random.Range(0f, 2f), 0, Random.Range(0f, 2f)));
            }
        }
    }

    void FollowPlayer()
    {
        wasNotFollowing = true;
        if (Vector3.Distance(transform.position, player.position) >= stopFollowPlayerDistance)
        {
            agent.isStopped = false;
            agent.SetDestination(player.position);
        }
        else
        {
            bool seePlayer = false;
            RaycastHit hit;

            Physics.Raycast(transform.position, player.position - transform.position, out hit, 15);
            if (hit.collider.gameObject == player.gameObject)
                seePlayer = true;

            if (seePlayer)
                agent.isStopped = true;
            else
            {
                agent.isStopped = false;
                agent.SetDestination(player.position);
            }

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position, transform.up), 1f);
        }
    }
}
