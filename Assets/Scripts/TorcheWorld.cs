﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TorcheWorld : MonoBehaviour
{
    public Transform uiPos;

    public TextMeshProUGUI text;
    
    void Start()
    {
        text.gameObject.transform.SetParent(Gears.gears.uiManager.textHolder.transform);
    }
    
    void Update()
    {
        if (text.gameObject.activeSelf)
        {
            text.gameObject.GetComponent<RectTransform>().position = Gears.gears.cam.WorldToScreenPoint(uiPos.position);

            if (Input.GetKeyDown(KeyCode.T))
            {
                Gears.gears.PlayerEquipment.AddEquipment(new TorchEquipment(Gears.gears.PlayerEquipment));
                Destroy(text.gameObject);
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            text.gameObject.SetActive(true);
        }
    }
    
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            text.gameObject.SetActive(false);
        }
    }
}
