﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShurikenScript : MonoBehaviour
{
    public int damage = 1;
    private bool hit = false;

    private bool detectCollision;
    private Vector3 dir;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyController>() && !hit)
        {
            hit = true;
            other.GetComponent<EnemyController>().Damage(damage, transform.position - other.transform.position);
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (!detectCollision) dir = GetComponent<Rigidbody>().GetPointVelocity(transform.position);
    }


    private void OnCollisionEnter(Collision other)
    {
        if (!detectCollision)
        {
            detectCollision = true;
        
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            transform.position += dir / 400;
            
            Destroy(gameObject, 60);
        }
    }
}
