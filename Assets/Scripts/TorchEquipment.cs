﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchEquipment : Equipment
{
    public float useRange = 4;
    
    public TorchEquipment(Player_Equipment playerEquipment) : base(playerEquipment)
    {
        equipmentName = "Torche";
    }
    public override void UseEquipment()
    {
        Collider[] explosionBarrelsInRange = Physics.OverlapSphere(playerEquipment.transform.position, useRange, Gears.gears.barrelLayer);

        foreach (var barrel in explosionBarrelsInRange)
        {
            //barrel.GetComponent<ExplosionBarrel>().Explode(1);
            barrel.GetComponent<ExplosionBarrel>().Explode();
        }
    }
}
