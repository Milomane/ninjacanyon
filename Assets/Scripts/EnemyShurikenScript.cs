﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShurikenScript : MonoBehaviour
{
    public int damage = 1;
    public int knockbackForce = 10;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerHealth>())
        {
            other.GetComponent<PlayerHealth>().Damage(damage, transform.position, knockbackForce);
            Destroy(gameObject);
        }
    }
}
