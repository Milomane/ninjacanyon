﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player_Equipment : MonoBehaviour
{
    public Equipment currentEquipment;
    
    public List<Equipment> equipments = new List<Equipment>();

    public int currentEquipmentIndex = 0;
    
    void Start()
    {
        Gears.gears.PlayerEquipment = this;
        Gears.gears.cam = GetComponentInChildren<Camera>();
        AddEquipment(new Shuriken_Equipment(this));
    }
    
    void Update()
    {
        currentEquipmentIndex += (int) Input.mouseScrollDelta.y;
        
        if (currentEquipmentIndex > equipments.Count - 1)
        {
            currentEquipmentIndex = 0;
        }else if (currentEquipmentIndex < 0)
        {
            currentEquipmentIndex = equipments.Count - 1;
        }

        //Debug.Log("Index : " + currentEquipmentIndex + " count : " + equipments.Count);
        if (equipments.Capacity > 0 && currentEquipment != equipments[currentEquipmentIndex])
        {
            currentEquipment = equipments[currentEquipmentIndex];
            Debug.Log("Change equipment " + currentEquipment);
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Use equipment");
            currentEquipment?.UseEquipment();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            //Test
            AddEquipment(new TorchEquipment(this));
        }
        
        Debug.Log(currentEquipmentIndex + " : " + equipments.Count);
    }

    public void AddEquipment(Equipment equipment)
    {
        equipments.Add(equipment);

        GameObject equipmentDisplay = Instantiate(Gears.gears.equipmentName, Gears.gears.uiManager.uiPlayerEquipmentsHolder.transform);
        equipmentDisplay.GetComponentInChildren<TextMeshProUGUI>().text = equipment.equipmentName;
        
        Gears.gears.UiPlayerEquipment.equipmentsDisplay.Add(equipmentDisplay);
    }
}
