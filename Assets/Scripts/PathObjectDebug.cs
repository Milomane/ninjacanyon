﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathObjectDebug : MonoBehaviour
{
    public float randomRadius = 2;
    public bool debugGizmo = false;
    
    private void OnDrawGizmos()
    {
        if (debugGizmo)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, randomRadius);
        }
    }
}
