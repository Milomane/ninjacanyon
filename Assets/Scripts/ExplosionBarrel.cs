﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ExplosionBarrel : MonoBehaviour
{
    public GameObject toDestroy;
    
    public TextMeshProUGUI text;
    
    public Transform uiPos;

    public ParticleSystem particlesSmoke;
    public ParticleSystem particlesExplosion;
    
    void Start()
    {
        text.gameObject.transform.SetParent(Gears.gears.uiManager.textHolder.transform);
    }
    
    void Update()
    {
        if (text.gameObject.activeSelf)
        {
            text.gameObject.GetComponent<RectTransform>().position = Gears.gears.cam.WorldToScreenPoint(uiPos.position);
        }
    }

    private IEnumerator ExplodeEnumerator(float timeBeforExplosion)
    {
        particlesSmoke?.Play();

        yield return new WaitForSeconds(timeBeforExplosion);
        
        particlesExplosion?.Play();
        Destroy(toDestroy, particlesExplosion.main.duration);
        Destroy(text, particlesExplosion.main.duration);
        Destroy(gameObject, particlesExplosion.main.duration);
    }

    public void Explode()
    {
        StartCoroutine(ExplodeEnumerator(particlesSmoke.main.duration));
    }
    
    /*public void Explode(float timeBeforExplosion)
    {
        Destroy(text, timeBeforExplosion);
        Destroy(toDestroy, timeBeforExplosion);
        Destroy(gameObject, timeBeforExplosion);
    }*/
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            text.gameObject.SetActive(true);
        }
    }
    
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            text.gameObject.SetActive(false);
        }
    }
}
