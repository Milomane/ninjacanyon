﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerEquipment : MonoBehaviour
{
    private float progression;

    private float lateProgression;

    public float radius;

    private float height2;
    private float width2;

    private float rangeBetweenEquipment;

    public List<GameObject> equipmentsDisplay = new List<GameObject>();
    
    private float alpha = 1;

    public GameObject selectGo;
    
    void Start()
    {
        Gears.gears.UiPlayerEquipment = this;
        
        width2 = Screen.width * radius;
        height2 = Screen.width * radius;
        
        selectGo.GetComponent<RectTransform>().position = new Vector2(GetComponent<RectTransform>().position.x + Mathf.Cos(Mathf.PI * 0.5f) * width2, 
            GetComponent<RectTransform>().position.y + Mathf.Sin(Mathf.PI * 0.5f) * height2);
    }
    
    void Update()
    {
        if (equipmentsDisplay.Capacity > 0)
        {
            //Debug.Log(Gears.gears.PlayerEquipment.currentEquipmentIndex);
            
            progression = Gears.gears.PlayerEquipment.currentEquipmentIndex * (Mathf.PI * 2 * 0.125f);

            lateProgression = Mathf.Lerp(lateProgression, progression, 0.1f);

            float diff = 0;
            
            alpha += Mathf.Abs(Input.mouseScrollDelta.y);
            alpha -= 0.01f;
            alpha = Mathf.Clamp(alpha, 0f, 1f);
            
            selectGo.GetComponent<Image>().color = new Color(selectGo.GetComponent<Image>().color.r,
                selectGo.GetComponent<Image>().color.g, selectGo.GetComponent<Image>().color.b, alpha);

            for (int i = 0; i < equipmentsDisplay.Count; i++)
            {
                equipmentsDisplay[i].GetComponent<Image>().color = new Color(1, 1, 1, 0);
                equipmentsDisplay[i].GetComponentInChildren<TextMeshProUGUI>().color = new Color(0, 0, 0, 0);
                
                float x = Mathf.Cos(lateProgression + diff + Mathf.PI * 0.5f) * width2;
                float y = Mathf.Sin(lateProgression + diff + Mathf.PI * 0.5f) * height2;
            
                equipmentsDisplay[i].GetComponent<RectTransform>().position = new Vector2(GetComponent<RectTransform>().position.x + x, GetComponent<RectTransform>().position.y + y);

                diff -= Mathf.PI * 2 * 0.125f;

                if (i <= Gears.gears.PlayerEquipment.currentEquipmentIndex + 4 &&
                    i >= Gears.gears.PlayerEquipment.currentEquipmentIndex - 3)
                {
                    /*if (lateProgression + diff <= 0f) {
                        equipmentsDisplay[i].GetComponent<RectTransform>().sizeDelta = new Vector2(180,50);
                    }
                    else {
                        equipmentsDisplay[i].GetComponent<RectTransform>().sizeDelta = new Vector2(160,30);
                    }*/

                    equipmentsDisplay[i].GetComponent<Image>().color = new Color(1, 1, 1, alpha);
                    equipmentsDisplay[i].GetComponentInChildren<TextMeshProUGUI>().color = new Color(0, 0, 0, alpha);
                }
            }
        }
    }

    public void GetDistanceBetweenButtons()
    {
        float perfectDistanceBetweenButtons = 2 * Mathf.PI / equipmentsDisplay.Count;

        rangeBetweenEquipment = perfectDistanceBetweenButtons;
    }
}
